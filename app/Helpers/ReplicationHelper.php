<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class ReplicationHelper
{
    public static function masterStatus() {
        if ( $status = DB::connection('master')->select('SHOW MASTER STATUS') )
        	return $status[0];
        return false;
    }

    public static function slaveHosts() {
    	if ( $hosts = DB::connection('master')->select('SHOW SLAVE HOSTS') )
        	return $hosts;
        return false;
    }

    public static function slaveStatus($slave) {
    	if ( $status = DB::connection($slave)->select('SHOW SLAVE STATUS') )
        	return $status[0];
        return false;
    }
}