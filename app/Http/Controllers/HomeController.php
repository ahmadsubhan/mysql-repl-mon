<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Helpers\ReplicationHelper;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'Home';

        // master status
        $masterStatus = ReplicationHelper::masterStatus();

        // slave hosts
        $slaveHosts = ReplicationHelper::slaveHosts();

        // slaves status
        $slaveNumber = 1;
        $slavesStatus = [];
        while($host = env('DB_HOST_SLAVE' . $slaveNumber)) {
            $slavesStatus[$host] = ReplicationHelper::slaveStatus('slave' . $slaveNumber);
            $slaveNumber++;
        }

        // slaves status summary
        $slavesStatusSummary = [];
        // means replication status is GOOD
        $overallStatus = true; 
        $summaryFields = ['Slave_IO_Running', 'Slave_SQL_Running'];
        // summary fields only if the slave is in trouble
        $summaryFieldsOpt = ['Last_Errno', 'Last_Error', 'Last_IO_Errno', 'Last_IO_Error', 'Last_SQL_Errno', 'Last_SQL_Error'];
        foreach($slavesStatus as $host => $status) {
            $summary = ['Host' => $host];

            foreach($summaryFields as $field)
                $summary[ $field ] = $status->{$field};

            if ( 'No' == $summary['Slave_IO_Running'] || 'No' == $summary['Slave_SQL_Running'] ) {
                // replication is in trouble
                $overallStatus = false;
                foreach($summaryFieldsOpt as $field)
                    $summary[ $field ] = $status->{$field};
            }

            $slavesStatusSummary[] = $summary;
        }

        // data
        $data['masterStatus'] = $masterStatus;
        $data['slaveHosts'] = $slaveHosts;
        $data['slavesStatus'] = $slavesStatus;
        $data['slavesStatusSummary'] = $slavesStatusSummary;
        $data['overallStatus'] = $overallStatus;

        return view('home', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
