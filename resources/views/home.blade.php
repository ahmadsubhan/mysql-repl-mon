@extends('layouts.front')

@section('content')
	<div class="row">
		<!-- left side -->
		<div class="col-md-6">
			<!-- Summary -->
			<div class="panel panel-{{ $overallStatus ? 'success' : 'danger' }}">
				<div class="panel-heading">
					<h3 class="panel-title">Summary Info</h3>
				</div>
				<div class="panel-body">
					<p>This is summary information of replication status.</p>
					<p>Overall status is <span class="label label-{{ $overallStatus ? 'success' : 'danger' }}">{{ $overallStatus ? 'GOOD' : 'NOT GOOD' }}</span></p>
					<h4>Slave Hosts</h4>
					<table class="table table-condensed">
						<thead>
							<tr>
								<th>Server_id</th>
								<th>Host</th>
								<th>Port</th>
								<th>Master_id</th>
								<th>Slave_UUID</th>
							</tr>
						</thead>
						<tbody>
						@foreach ($slaveHosts as $host)
							<tr>
								<td>{{ $host->Server_id }}</td>
								<td>{{ $host->Host }}</td>
								<td>{{ $host->Port }}</td>
								<td>{{ $host->Master_id }}</td>
								<td>{{ $host->Slave_UUID }}</td>
							</tr>
						@endforeach
						</tbody>
					</table>

					<h4>Slaves Status Summary</h4>
					@foreach ($slavesStatusSummary as $summary)
					<table class="table table-condensed">
						<tbody>
						@foreach ($summary as $key => $val)
						    <tr>
						    	<td><strong>{{ $key }}</strong></td><td>:</td><td>{{ $val }}</td>
						    </tr>
						@endforeach
						</tbody>
					</table>
					@endforeach
				</div>
			</div>
			<!-- Master Status -->
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Master Status</h3>
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<tbody>
						@foreach ($masterStatus as $key => $val)
						    <tr>
						    	<td><strong>{{ $key }}</strong></td><td>:</td><td>{{ $val }}</td>
						    </tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- right side -->
		<div class="col-md-6">
		@foreach ($slavesStatus as $index => $slaveStatus)
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">Slave Status: {{ $index }}</h3>
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<tbody>
						@foreach ($slaveStatus as $key => $val)
						    <tr>
						    	<td><strong>{{ $key }}</strong></td>
						    	<td>:</td>
						    	<td>{{ starts_with($key, 'Replicate_') ? str_replace(',', ', ', $val) : $val }}</td>
						    </tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		@endforeach
		</div>
	</div>
@endsection