<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>MySQL Replication Monitor - {{ $title }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:200,400,600" rel="stylesheet" type="text/css">

        <!-- Bootstrap -->
	    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->

	    <link href="css/front.css" rel="stylesheet">
	    @yield('head')
    </head>
    <body>
	    <!-- header -->
		<div class="header">
			<nav class="navbar navbar-inverse navbar-fixed-top">
		      <div class="container">
		        <div class="navbar-header">
		          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		          </button>
		          <a class="navbar-brand" href="#">MySQL Replication Monitor</a>
		        </div>
		        <div id="navbar" class="collapse navbar-collapse">
		          <ul class="nav navbar-nav">
		          	@section('navigation')
		            <li class="active"><a href="/">Home</a></li>
		            <li><a href="/config">Config</a></li>
		            @show
		          </ul>
		        </div><!--/.nav-collapse -->
		      </div>
		    </nav>
		</div>

		<!-- container -->
		<div class="container">
			<div class="content">
		    	@yield('content')
		    </div>

			<!-- footer -->
			<div class="footer">
				@section('footer')
				<hr>
				<small><strong>MySQL Replication Monitor Tool</strong>. Copyright &copy; 2016 - <a href="mailto:ahmad.subhan@aol.com">Ahmad Subhan</a>. Powered By <a href="https://laravel.com/" target="_blank">Laravel</a>.</small>
				@show
			</div>
		</div>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="bower_components/jquery/dist/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    </body>
</html>